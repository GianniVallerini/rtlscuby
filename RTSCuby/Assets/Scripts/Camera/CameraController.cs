﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    #region Fields

    public GameObject cameraAnchor;

    public KeyCode moveUp;
    public KeyCode moveDown;
    public KeyCode moveLeft;
    public KeyCode moveRight;
    public KeyCode speedUpMovement;
    public KeyCode rotateLeft;
    public KeyCode rotateRight;
    public KeyCode rotateUp;
    public KeyCode rotateDown;
    public KeyCode mouseHoldToRotate;
    public bool invertedScroll = false;
    public bool invertedVRotation = false; 

    public float movementSpeed = .1f;
    public float horizontalRotationSpeed = 60f;
    public float verticalRotationSpeed = 45f;
    public float zoomSpeed = 1f;
    public float minZoom = 10f;
    public float maxZoom = 20f;
    public float cameraSpeed = 3;
    public float distanceFactor = .8f;

    private float horizontalMovement = 0;
    private float verticalMovement = 0;
    private float horizontalRotationInput = 0;
    private float verticalRotationInput = 0;
    private float zoomMovement = 0;
    private float currZoom = 10f;
    private float currVertRotation = 0;
    private float minVertRotation = -25f;
    private float maxVertRotation = 25f;

    private WorldGenerator worldGenerator;
    private WorldTerrain worldTerrain;
    private int currX = 0;
    private int currZ = 0;

    private Vector3 targetPos = Vector3.zero;
    private float minDistanceFromTarget = 1f;
    private float distanceFromTarget = 0;

    #endregion

    #region Unity Callbacks

    private void Start()
    {
        worldGenerator = FindObjectOfType<WorldGenerator>();
        if(worldGenerator != null)
        {
            worldGenerator.TerrainGenerated += GetWorldTerrain;
        }
        else
        {
            Debug.LogWarning("Missing world generator in Camera Controller");
        }

        targetPos = transform.position;
        currZoom = Mathf.Lerp(minZoom, maxZoom, .5f);
        cameraAnchor.transform.localPosition = cameraAnchor.transform.forward * -currZoom; 
    }

    private void Update()
    {
        CheckMovementKeys();
        if (worldTerrain != null)
        {
            LevelCameraController();
        }

        MoveTowardsTargetPos();
    }

    #endregion

    #region Methods

    private void CheckMovementKeys()
    {
        horizontalMovement = 0;
        verticalMovement = 0;
        horizontalRotationInput = 0;
        verticalRotationInput = 0;
        zoomMovement = 0;

        #region Movement

        bool boostedMovement = Input.GetKey(speedUpMovement);

        if (Input.GetKey(moveUp))
        {
            verticalMovement++;
        }
        if (Input.GetKey(moveDown))
        {
            verticalMovement--;
        }

        if (Input.GetKey(moveRight))
        {
            horizontalMovement++;
        }
        if (Input.GetKey(moveLeft))
        {
            horizontalMovement--;
        }

        Vector3 hMovement = transform.right * horizontalMovement * movementSpeed * Time.deltaTime;
        Vector3 vMovement = transform.forward * verticalMovement * movementSpeed * Time.deltaTime;

        hMovement = boostedMovement ? hMovement * 3 : hMovement;
        vMovement = boostedMovement ? vMovement * 3 : vMovement;

        targetPos += hMovement;
        targetPos += vMovement;

        currX = Mathf.FloorToInt(transform.position.x);
        currZ = Mathf.FloorToInt(transform.position.z);

        #endregion

        #region Zoom

        zoomMovement = -Input.mouseScrollDelta.y;
        if (invertedScroll)
        {
            zoomMovement = -zoomMovement;
        }

        if ((zoomMovement > 0 && currZoom < maxZoom) || (zoomMovement < 0 && currZoom > minZoom))
        {
            currZoom += zoomMovement * zoomSpeed * Time.deltaTime;
            currZoom = Mathf.Clamp(currZoom, minZoom, maxZoom);
            cameraAnchor.transform.position = transform.position - (cameraAnchor.transform.forward * currZoom);
        }

        #endregion

        #region Rotation

        if (Input.GetKey(mouseHoldToRotate))
        {
            horizontalRotationInput = Input.GetAxis("Mouse X") * Time.deltaTime * (horizontalRotationSpeed);
            verticalRotationInput = -Input.GetAxis("Mouse Y") * Time.deltaTime * (verticalRotationSpeed);
        }
        else
        {
            if (Input.GetKey(rotateRight))
            {
                horizontalRotationInput++;
            }
            if (Input.GetKey(rotateLeft))
            {
                horizontalRotationInput--;
            }

            if (Input.GetKey(rotateUp))
            {
                verticalRotationInput++;
            }
            if (Input.GetKey(rotateDown))
            {
                verticalRotationInput--;
            }
        }

        if (invertedVRotation)
        {
            verticalRotationInput = -verticalRotationInput;
        }

        if(verticalRotationInput > 1 || verticalRotationInput < -1)
        {
            verticalRotationInput = Mathf.Sign(verticalRotationInput);
        }

        transform.Rotate(Vector3.up, horizontalRotationInput * Time.deltaTime * horizontalRotationSpeed, Space.World);

        float deltaVertRotation = verticalRotationInput * Time.deltaTime * verticalRotationSpeed;
        if ((deltaVertRotation > 0 && currVertRotation < maxVertRotation - (deltaVertRotation * 1.1f)) ||
            (deltaVertRotation < 0 && currVertRotation > minVertRotation + (deltaVertRotation * 1.1f)))
        {
            currVertRotation += deltaVertRotation;
            transform.Rotate(Vector3.right * deltaVertRotation, Space.Self);
        }

        #endregion
    }

    private void LevelCameraController()
    {
        bool groundHeightFound = false;
        if (currX > 0 && currX < worldGenerator.worldStats.sizeX &&
               currZ > 0 && currZ < worldGenerator.worldStats.sizeZ)
        {
            for (int y = worldGenerator.worldStats.maxTerrainHeight - 1; !groundHeightFound && y >= 0; y--)
            {
                if (worldTerrain.matrix[currX, y, currZ].id != 0)
                {
                    targetPos = new Vector3(targetPos.x, y + 1, targetPos.z);
                    groundHeightFound = true;
                }
            }
        }
    }

    private void GetWorldTerrain(WorldTerrain terrain)
    {
        worldTerrain = terrain;
    }

    private void MoveTowardsTargetPos()
    {
        if(Vector3.Distance(transform.position, targetPos) > minDistanceFromTarget)
        {
            Vector3 direction = targetPos - transform.position;
            distanceFromTarget = direction.magnitude;

            transform.position += direction.normalized * cameraSpeed * (distanceFromTarget * distanceFactor) * Time.deltaTime;
        }
    }

    #endregion
}
