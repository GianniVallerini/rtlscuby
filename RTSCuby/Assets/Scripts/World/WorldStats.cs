﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WorldStats", menuName = "New World Stats")]
public class WorldStats : ScriptableObject
{
    public int sizeX = 128;
    public int sizeZ = 128;
    public float scale = 15f;
    public int maxTerrainHeight = 64;
    public int grassLevels = 1;
    public int dirtLevels = 3;
    [Range(0, 25)] public int numberOfCitizens = 1;
    [Range(0f, 1f)] public float foodChance = .3f;
    [Range(3f, 8f)] public float foodMass = 5f;
    public int seed = 0;

    public WorldTerrain GenerateWorldTerrain()
    {
        WorldTerrain worldTerrain = new WorldTerrain
        {
            matrix = new CubeDataRef[sizeX, maxTerrainHeight, sizeZ],
            maxBounds = new Vector3(sizeX, maxTerrainHeight, sizeZ),
        };

        Random.InitState(seed);
        int terrainNoiseSeed = Random.Range(0, 10000000);
        int entitiesNoiseSeed = Random.Range(10000000, 20000000);

        GenerateTerrainMap(terrainNoiseSeed, worldTerrain);
        GenerateEntitiesMap(entitiesNoiseSeed, worldTerrain);
        InitializeTerrainCubeVariables(terrainNoiseSeed, worldTerrain);

        return worldTerrain;
    }

    [Button("Generate a Random Seed")]
    private void GenerateSeed()
    {
        seed = Random.Range(10000000, 99999999);
    }

    private void GenerateTerrainMap(int seed, WorldTerrain worldTerrain)
    {
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeZ; j++)
            {
                int terrainHeight = (int)Mathf.Lerp(0, maxTerrainHeight - 1,
                                    Mathf.PerlinNoise((i + seed) / scale, (j + seed) / scale));
                for (int k = 0; k < maxTerrainHeight; k++)
                {
                    worldTerrain.matrix[i, k, j] = new CubeDataRef();

                    if (k <= terrainHeight)
                    {
                        if (k > terrainHeight - grassLevels)
                        {
                            worldTerrain.matrix[i, k, j].id = 3; //grass
                        }
                        else if (k > terrainHeight - grassLevels - dirtLevels)
                        {
                            worldTerrain.matrix[i, k, j].id = 2; //dirt
                        }
                        else
                        {
                            worldTerrain.matrix[i, k, j].id = 1; //stone
                        }
                    }
                    else
                    {
                        worldTerrain.matrix[i, k, j].id = 0; //air
                    }
                }
            }
        }
    }

    private void GenerateEntitiesMap(int seed, WorldTerrain worldTerrain)
    {
        GenerateFood(seed, worldTerrain);
        GenerateSpawnPoints(worldTerrain);
    }

    private void GenerateFood(int seed, WorldTerrain worldTerrain)
    {
        bool surfaceFound = false;
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeZ; j++)
            {
                surfaceFound = false;
                if (Mathf.PerlinNoise((i + seed) / foodMass, (j + seed) / foodMass) < (foodChance * .4f))
                {
                    for (int k = 0; !surfaceFound && k < maxTerrainHeight; k++)
                    {
                        if (worldTerrain.matrix[i, k, j].id == 0)
                        {
                            surfaceFound = true;
                            worldTerrain.matrix[i, k, j].id = 4; //cherry bush
                        }
                    }
                }
            }
        }
    }

    private void GenerateSpawnPoints(WorldTerrain worldTerrain)
    {
        bool surfaceFound = false;
        int citizenSpawnerQuadWidth = 0;
        int spawnedCitizens = 0;
        if (numberOfCitizens <= 1)
        {
            citizenSpawnerQuadWidth = 0;
        }
        else if (numberOfCitizens <= 9)
        {
            citizenSpawnerQuadWidth = 1;
        }
        else if (numberOfCitizens <= 25)
        {
            citizenSpawnerQuadWidth = 2;
        }
        for (int i = sizeX / 2 - citizenSpawnerQuadWidth; spawnedCitizens < numberOfCitizens &&
             i <= sizeX / 2 + citizenSpawnerQuadWidth; i++)
        {
            for (int j = sizeZ / 2 - citizenSpawnerQuadWidth; spawnedCitizens < numberOfCitizens &&
                 j <= sizeZ / 2 + citizenSpawnerQuadWidth; j++)
            {
                surfaceFound = false;
                for (int k = 0; spawnedCitizens < numberOfCitizens && !surfaceFound && k < maxTerrainHeight; k++)
                {
                    if (worldTerrain.matrix[i, k, j].id == 0)
                    {
                        surfaceFound = true;
                        worldTerrain.matrix[i, k, j].id = -1; //citizen spawner
                        spawnedCitizens++;
                    }
                }
            }
        }
    }

    private void InitializeTerrainCubeVariables(int seed, WorldTerrain worldTerrain)
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int z = 0; z < sizeZ; z++)
            {
                for (int y = 0; y < maxTerrainHeight; y++)
                {
                    worldTerrain.matrix[x, y, z].InitializeVariables();
                }
            }
        }
    }
}

[System.Serializable]
public class WorldTerrain
{
    public CubeDataRef[,,] matrix;
    public Vector3 maxBounds = Vector3.zero;
}
