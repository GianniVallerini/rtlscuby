﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class WorldGenerator : MonoBehaviour
{
    #region Fields

    public event Action<WorldTerrain> TerrainGenerated;

    public Transform world;
    public GameObject worldChunk;

    public WorldStats worldStats;
    [HideInInspector] public WorldTerrain terrainGenerated;

    private List<Chunk> generatedChunks = new List<Chunk>();

    #endregion

    #region Unity Callbacks

    private void OnDrawGizmos()
    {
        if (world != null)
        {
            Color gizmoColor = worldStats != null ? Color.green : Color.red;
            gizmoColor.a = .6f;
            Gizmos.color = gizmoColor;

            Gizmos.DrawSphere(world.position, .5f);
        }
    }

    private void Start()
    {
        StartCoroutine(DelayedGeneration());
    }

    #endregion

    #region Methods

    private IEnumerator DelayedGeneration()
    {
        yield return new WaitForSeconds(.3f);
        StartCoroutine(GenerateWorld());
    }

    private IEnumerator GenerateWorld()
    {
        WorldTerrain terrain = worldStats.GenerateWorldTerrain();

        int totalChunkToCreate = (worldStats.sizeX / VoxelData.chunkWidth) *
                                 (worldStats.sizeZ / VoxelData.chunkWidth) *
                                 (worldStats.maxTerrainHeight / VoxelData.chunkHeight);
        int chunksCreated = 0;
        float progress = chunksCreated / (float)totalChunkToCreate;

#if UNITY_EDITOR
        UILoadingBar.instance.DisplayProgressBar("Creating World", "Initializing", progress);
        yield return new WaitForSeconds(.1f);
#endif

        if (worldStats != null)
        {
            for(int x = 0; x < worldStats.sizeX / VoxelData.chunkWidth; x++)
            {
                for(int z = 0; z < worldStats.sizeZ / VoxelData.chunkWidth; z++)
                {
                    for (int y = 0; y < worldStats.maxTerrainHeight / VoxelData.chunkHeight; y++)
                    {
#if UNITY_EDITOR
                        UILoadingBar.instance.DisplayProgressBar("Creating World", "chunk_" + chunksCreated + " of " + totalChunkToCreate, progress);
#endif
                        GameObject currentChunk = Instantiate(worldChunk, world);
                        currentChunk.name = "Chunk_" + chunksCreated;
                        Chunk currChunkScript = currentChunk.GetComponent<Chunk>();
                        generatedChunks.Add(currChunkScript);
                        Vector3 chunkOffset = new Vector3(x * VoxelData.chunkWidth,
                                                          y * VoxelData.chunkHeight,
                                                          z * VoxelData.chunkWidth);
                        currentChunk.transform.position = chunkOffset;
                        currChunkScript.Initialize(chunkOffset, terrain);
                        chunksCreated++;
                        progress = chunksCreated / (float)totalChunkToCreate;
                        yield return null;
                    }
                }
            }
#if UNITY_EDITOR
            UILoadingBar.instance.DisplayProgressBar("Creating World", "Completing", 1);
            yield return new WaitForSeconds(.1f);
            UILoadingBar.instance.ClearProgressBar();
            yield return null;
#endif
        }
        else
        {
            Debug.LogWarning("Missing world stats in world Generator");
        }

        terrainGenerated = terrain;
        TerrainGenerated?.Invoke(terrainGenerated);
        ActivateChunks();
    }

    private void ActivateChunks()
    {
        foreach(Chunk chunk in generatedChunks)
        {
            chunk.ActivateSurfaceCubes();
        }
    }

#endregion
}
