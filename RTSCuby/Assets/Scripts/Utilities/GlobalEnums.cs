﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Axis
{
    X,
    Y,
    Z,
}

public enum JobZoneType
{
    Mining,
    Gathering,
}

public enum SelectableType
{
    Cube,
    JobZone,
    JobZoneButton,
    Entity,
}

public enum SelectionState
{
    Free,
    ModifyingJobZone,
    PlacingNewJobZone,
}
