﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEditor;

[CreateAssetMenu(fileName = "n_cubename", menuName = "New Cube Data/Generic Interactable")]
public class InteractableCubeData : CubeData
{
    #region Fields

    #endregion

    #region Unity Callbacks

    #endregion

    #region Methods

    public override void Initialize(Vector3 worldPosition)
    {
        base.Initialize(worldPosition);
    }

    public override void InitializeVariables()
    {
        base.InitializeVariables();
    }

    public virtual void Interact(Entity interactor)
    {
        Debug.Log(interactor + " is interacting with " + nomenclature);
    }

    #endregion
}
