﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEditor;

[CreateAssetMenu(fileName = "n_cubename", menuName = "New Cube Data/Food")]
public class InteractFoodCubeData : InteractableCubeData
{
    #region Fields

    public int minFoodAmount = 100;
    public int maxFoodAmount = 200;
    public int foodPerInteract = 5;

    #endregion

    #region Unity Callbacks

    #endregion

    #region Methods

    public override void Initialize(Vector3 worldPosition)
    {
        base.Initialize(worldPosition);
    }

    public override void InitializeVariables()
    {
        base.InitializeVariables();
        cubeVariables.resources = Random.Range(minFoodAmount, maxFoodAmount);
    }

    public override void Interact(Entity interactor)
    {
        base.Interact(interactor);
    }

    #endregion
}
