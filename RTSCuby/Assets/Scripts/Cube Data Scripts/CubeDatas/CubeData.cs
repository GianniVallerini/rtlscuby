﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEditor;

[CreateAssetMenu(fileName = "n_cubename", menuName = "New Cube Data/Generic")]
public class CubeData : ScriptableObject
{
    #region Fields

    public string nomenclature = "";
    public bool isSolid = true;
    public bool isDrawable = true;
    public bool isTargetable = true;
    public int hitPoints = 100;

    public int backTextureID = 0;
    public int frontTextureID = 0;
    public int topTextureID = 0;
    public int botTextureID = 0;
    public int leftTextureID = 0;
    public int rightTextureID = 0;

    [HideInInspector] public CubeVariables cubeVariables = new CubeVariables();

    #endregion

    #region Unity Callbacks

    #endregion

    #region Methods

    public int GetTextureID(int faceIndex)
    {
        //back, front. top, bot, left, right
        switch(faceIndex)
        {
            case 0:
                return backTextureID;
            case 1:
                return frontTextureID;
            case 2:
                return topTextureID;
            case 3:
                return botTextureID;
            case 4:
                return leftTextureID;
            case 5:
                return rightTextureID;
            default:
                Debug.LogError("Erron in GetTextureID: invalid faceIndex -> " + faceIndex);
                return 0;
        }
    }

    public CubeVariables GetVariables()
    {
        InitializeVariables();
        return cubeVariables;
    }

    public virtual void InitializeVariables()
    {
        cubeVariables.hitPoints = hitPoints;
    }

    public virtual void Initialize(Vector3 worldPosition) { }

    #endregion
}

public class CubeDataRef
{
    public short id = 0;

    public CubeVariables vars = new CubeVariables();

    public void InitializeVariables()
    {
        vars = Database.Instance.cubesDatas[id].GetVariables().Copy();
    }
}