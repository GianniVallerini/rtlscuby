﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEditor;

[CreateAssetMenu(fileName = "n_cubename", menuName = "New Cube Data/Spawn Point/Generic Spawn Point")]
public class SpawnPointCube : CubeData
{
    #region Fields

    public GameObject spawnedObject;
    public bool isCentred = true;

    [HideInInspector] public GameObject currSpawnedObject;

    #endregion

    #region Unity Callbacks

    #endregion

    #region Methods

    public override void Initialize(Vector3 worldPosition)
    {
        base.Initialize(worldPosition);
        GameObject objectSpawned = Instantiate(spawnedObject, worldPosition, Quaternion.identity);
        currSpawnedObject = objectSpawned;
    }

    #endregion
}
