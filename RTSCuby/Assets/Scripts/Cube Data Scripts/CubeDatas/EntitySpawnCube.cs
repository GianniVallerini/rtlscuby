﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEditor;

[CreateAssetMenu(fileName = "n_cubename", menuName = "New Cube Data/Spawn Point/Entity Spawn Point")]
public class EntitySpawnCube : SpawnPointCube
{
    #region Fields

    #endregion

    #region Unity Callbacks

    #endregion

    #region Methods

    public override void Initialize(Vector3 worldPosition)
    {
        base.Initialize(worldPosition);
        Entity entitySpawned = currSpawnedObject.GetComponent<Entity>();
        int x = Mathf.FloorToInt(entitySpawned.transform.position.x);
        int y = Mathf.FloorToInt(entitySpawned.transform.position.y);
        int z = Mathf.FloorToInt(entitySpawned.transform.position.z);
        entitySpawned.pos = new int[3] {x,y,z};
    }

    #endregion
}
