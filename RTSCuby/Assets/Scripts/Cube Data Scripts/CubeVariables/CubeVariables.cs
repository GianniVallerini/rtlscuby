﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeVariables
{
    public int hitPoints = 0;
    public int resources = 0;

    public CubeVariables Copy()
    {
        CubeVariables copy = new CubeVariables();
        copy.hitPoints = hitPoints;
        copy.resources = resources;

        return copy;
    }

    public string ShowVars()
    {
        return "Hitpoints: " + hitPoints +
               "\nResources: " + resources; 
    }
}
