﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : Singleton<TimeManager>
{
    #region Time Events

    //Update number of events
    public event Action PreTick;
    public event Action Tick;
    public event Action PostTick;

    public event Action<bool> PauseSwitched;

    public float tickTime = 1f;

    private float currTickTime = 1f;
    private int numberOfEvents = 3;
    private int tickMultiplier = 1;
    private float eventTime = 0;

    #endregion

    #region Fields

    public bool isPaused = true;

    #endregion

    #region Unity Callbacks

    private void Start()
    {
        StartCoroutine(TickCoroutine());
    }

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            SwitchPause();
        }
    }

    #endregion

    #region Methods

    [Button("Switch Pause")]
    public void SwitchPause()
    {
        isPaused = !isPaused;
        PauseSwitched?.Invoke(isPaused);
    }

    public void SetTickMultiplier(int desiredMultiplier)
    {
        tickMultiplier = desiredMultiplier;
    }

    public float GetEventTime()
    {
        return eventTime;
    }

    private IEnumerator TickCoroutine()
    {
        while(true)
        {
            currTickTime = tickTime * tickMultiplier;
            eventTime = currTickTime / numberOfEvents;
            if (isPaused)
            {
                yield return null;
            }
            else
            {
                PreTick?.Invoke();
                yield return new WaitForSeconds(eventTime);
                Tick?.Invoke();
                yield return new WaitForSeconds(eventTime);
                PostTick?.Invoke();
                yield return new WaitForSeconds(eventTime);
            }
            yield return null;
        }
    }

    #endregion
}
