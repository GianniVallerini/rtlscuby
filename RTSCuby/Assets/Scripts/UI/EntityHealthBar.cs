﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EntityHealthBar : MonoBehaviour
{
    #region Fields

    [HideInInspector] public Entity linkedEntity;

    [SerializeField] private Slider barSlider;
    [SerializeField] private Image fill;
    [SerializeField] private Image background;
    [SerializeField, Range(0f,1f)] private float backgroundColorMultiplier = .8f;

    #endregion

    #region Unity Callbacks

    #endregion

    #region Methods

    public void Register()
    {
        if (linkedEntity != null)
        {
            linkedEntity.HealthChanged += SetValue;
            SetValue(linkedEntity.Health / linkedEntity.maxHealth);
        }
    }

    public void DeRegister()
    {
        if (linkedEntity != null)
            linkedEntity.HealthChanged -= SetValue;
    }

    public void SetValue(float normalizedValue)
    {
        barSlider.value = normalizedValue;
    }

    public void SetColor(Color color)
    {
        Color colorToSet = color;
        fill.color = colorToSet;
        colorToSet = new Color(colorToSet.r * backgroundColorMultiplier,
                               colorToSet.g * backgroundColorMultiplier,
                               colorToSet.b * backgroundColorMultiplier, backgroundColorMultiplier);
        background.color = colorToSet;

    }

    public void SetBarOverEntity()
    {
        transform.position = linkedEntity.transform.position + (Vector3.up * (linkedEntity.entityHeight + .4f));

        Vector3 center = new Vector3(0, transform.position.y, 0);
        Vector3 targetPos = Camera.main.transform.position;
        targetPos.y = transform.position.y;
        transform.LookAt(targetPos);
    }

    #endregion
}
