﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPause : MonoBehaviour
{
    #region Fields

    public Sprite pausedIcon;
    public Sprite unpausedIcon;
    public Image pausedImage;

    #endregion

    #region Unity Callbacks

    private void Start()
    {
        TimeManager.instance.PauseSwitched += UpdateGraphics;
        UpdateGraphics(TimeManager.instance.isPaused);
    }

    #endregion

    #region Methods

    private void UpdateGraphics(bool isPaused)
    {
        pausedImage.sprite = isPaused ? pausedIcon : unpausedIcon;
    }

    #endregion
}
