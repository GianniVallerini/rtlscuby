﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UILoadingBar : Singleton<UILoadingBar>
{
    #region Fields

    public TextMeshProUGUI loadingText;
    public Slider loadingSlider;
    public GameObject activationObject;

    #endregion

    #region Unity Callbacks

    private void Start()
    {
        ClearProgressBar();
    }

    #endregion

    #region Methods

    public void DisplayProgressBar(string title, string text, float normalizedValue)
    {
        SetActive(true);
        loadingText.text = title + "\n" + text;
        loadingSlider.value = normalizedValue;
    }

    public void ClearProgressBar()
    {
        SetActive(false);
    }

    public void SetActive(bool value)
    {
        activationObject.SetActive(value);
    }

    #endregion
}
