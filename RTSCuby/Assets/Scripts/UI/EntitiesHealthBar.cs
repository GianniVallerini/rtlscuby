﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitiesHealthBar : Singleton<EntitiesHealthBar>
{
    #region Fields

    public GameObject entityHealthBarPrefab;

    private List<EntityHealthBar> healthBars = new List<EntityHealthBar>();

    #endregion

    #region Unity Callbacks

    private void Update()
    {
        foreach(EntityHealthBar bar in healthBars)
        {
            bar.SetBarOverEntity();
        }
    }

    #endregion

    #region Methods

    public void RegisterNewHealthBar(Entity entity, Color barColor)
    {
        GameObject newBar = Instantiate(entityHealthBarPrefab, transform);
        EntityHealthBar barScript = newBar.GetComponent<EntityHealthBar>();
        barScript.linkedEntity = entity;
        barScript.SetColor(barColor);
        barScript.Register();
        healthBars.Add(barScript);
    }

    public void DeRegisterHealthBar(Entity entity)
    {
        EntityHealthBar barToRemove = healthBars.Find(bar => bar.linkedEntity == entity);
        if(barToRemove != null)
        {
            barToRemove.DeRegister();
            Destroy(barToRemove.gameObject);
            healthBars.Remove(barToRemove);
        }
    }

    #endregion
}
