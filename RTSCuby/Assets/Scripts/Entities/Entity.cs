﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    #region Fields

    public event Action<float> HealthChanged;
    public string nomenclature = "Entity";

    public bool isSelectable = true;
    public GameObject selectionToken;
    public GameObject selectableToken;

    public int entityWidth = 1;
    public int entityHeight = 1;
    public Vector3 positionOffset = new Vector3(.5f, .5f, .5f);
    public AnimationCurve jumpCurve;
    [Range(0f,2f)]public float jumpHeight = 1;

    public float maxHealth = 100f;
    private float health = 100;
    [ReadOnly] public float Health
    {
        get
        {
            return health;
        }
        set
        {
            if(health != value)
            {
                HealthChanged?.Invoke(value/maxHealth);
            }
            health = value;
        }
    }

    [HideInInspector] public int[] pos = new int[3] {0, 0, 0};

    //cubePos, interactableCubeData
    private Dictionary<Vector3, InteractableCubeData> interactableCubes = new Dictionary<Vector3, InteractableCubeData>();
    private WorldTerrain terrain;
    private EntitiesHealthBar healthBarManager;

    private int[] currentTargetPos = new int[3] {-1, -1, -1};
    private readonly int[] nullTargetPos = new int[3] { -1, -1, -1 };

    private EntityMovementDirection lastMovementDone;
    private EntityMovementDirection LastMovementDone
    {
        get
        {
            return lastMovementDone;
        }
        set
        {
            lastMovementDone = value;
            switch(lastMovementDone)
            {
                case EntityMovementDirection.Front:
                    transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                    break;
                case EntityMovementDirection.Back:
                    transform.rotation = Quaternion.Euler(0f, 180f, 0f);
                    break;
                case EntityMovementDirection.Up:
                    
                    break;
                case EntityMovementDirection.Down:
                    
                    break;
                case EntityMovementDirection.Right:
                    transform.rotation = Quaternion.Euler(0f, 90f, 0f);
                    break;
                case EntityMovementDirection.Left:
                    transform.rotation = Quaternion.Euler(0f, 270f, 0f);
                    break;
            }
        }
    }

    private List<ViewFace> facesToScan = new List<ViewFace>();

    #endregion

    #region Unity Callbacks

    private void Start()
    {
        WorldGenerator worldGenerator = FindObjectOfType<WorldGenerator>();
        if (worldGenerator != null)
        {
            terrain = worldGenerator.terrainGenerated;
            Scan();
        }
        
        Health = maxHealth;
        SetPosition(pos);

        healthBarManager = EntitiesHealthBar.instance;
        if(healthBarManager != null)
        {
            healthBarManager.RegisterNewHealthBar(this, Color.green);
        }

        TimeManager.instance.PreTick += Think;
        TimeManager.instance.Tick += Act;
        TimeManager.instance.PostTick += ReEvaluate;
    }

    private void OnDrawGizmos()
    { 
        //draw viewRange
        if (Settings.Instance.showEntitiesViewRange)
        {
            Gizmos.color = Settings.Instance.entitiesViewRangeColor;

            Gizmos.DrawCube(transform.position, new Vector3(RangeData.baseRange + 1f, RangeData.baseRange + 1f, RangeData.baseRange + 1f));
        }

        //draw all possible interactable cubes in viewRange
        if (Settings.Instance.showEntitiesPossibleTargets)
        {
            Gizmos.color = Settings.Instance.entitiesPossTargetsColor;

            foreach (KeyValuePair<Vector3, InteractableCubeData> pair in interactableCubes)
            {
                Gizmos.DrawLine(transform.position + GetEyesPos(), pair.Key + (Vector3.one / 2));
            }
        }

        //draw currTarget
        if (currentTargetPos[0] != nullTargetPos[0] && Settings.Instance.showEntitiesCurrentTarget)
        {
            Gizmos.color = Settings.Instance.entitiesCurrTargetColor;

            Gizmos.DrawLine(transform.position + GetEyesPos(), new Vector3(currentTargetPos[0] + .5f,
                                                                           currentTargetPos[1] + .5f,
                                                                           currentTargetPos[2] + .5f));
        }
    }

    #endregion

    #region Methods

    public virtual void Think()
    {

    }

    public virtual void Act()
    {
        bool possibleMovementFound = false;
        int[] possibleMovement = new int[3] { -1, -1, -1 }; 
        while(!possibleMovementFound)
        {
            int currGuess = UnityEngine.Random.Range(0, 6);
            if(currGuess != 2 && currGuess != 3 && CanMoveTo((EntityMovementDirection)currGuess, out possibleMovement))
            {
                Move(possibleMovement);
                LastMovementDone = (EntityMovementDirection)currGuess;
                possibleMovementFound = true;
            }
        }
    }

    public virtual void ReEvaluate()
    {
        foreach (ViewFace viewFace in facesToScan)
        {
            Scan(viewFace);
        }
    }

    public void SetSelected(bool value)
    {
        selectionToken.SetActive(value);
    }

    public void SetSelectable(bool value)
    {
        selectableToken.SetActive(value);
    }

    public virtual string ShowInfos()
    {
        return "Health: " + health;
    }

    //all around
    public void Scan()
    {
        interactableCubes = RangeData.Scan(pos, terrain);
    }

    //single cube
    public void Scan(Vector3 cubePosition)
    {
        interactableCubes = RangeData.Scan(pos, terrain, interactableCubes, cubePosition);
    }

    //a face
    public void Scan(ViewFace viewFace)
    {
        interactableCubes = RangeData.Scan(pos, terrain, interactableCubes, viewFace);
    }
    
    public Vector3 GetEyesPos()
    {
        return Vector3.up * entityHeight;
    }

    public void MoveTowardsTarget()
    {

    }

    public void Move(int[] movementPos)
    {
        SetPosition(movementPos);
    }

    public IEnumerator MoveCor(Vector3 endingPos)
    {
        float t = 0, a = 0, b = 0;
        Vector3 startingPos = transform.position;
        float maxT = TimeManager.instance.GetEventTime();
        while(t < maxT)
        {
            t += Time.deltaTime;
            a = t / maxT;
            b = jumpCurve.Evaluate(t / maxT);
            transform.position = new Vector3(Mathf.Lerp(startingPos.x, endingPos.x, a),
                                             Mathf.Lerp(startingPos.y, endingPos.y, a) + jumpHeight * b,
                                             Mathf.Lerp(startingPos.z, endingPos.z, a));
            yield return null;
        }

        transform.position = endingPos;

        yield return null;
    }

    public bool CanMoveTo(EntityMovementDirection dir, out int[] possibleMovementPos)
    {
        bool canMove = false;
        int[] movementPos = new int[3] { -1, -1, -1 };
        int[] checkedCubePos = EntityData.GetDirectionalPos(dir, pos);
        if (checkedCubePos[0] >= 0 && checkedCubePos[0] < terrain.maxBounds.x &&
            checkedCubePos[1] >= 0 && checkedCubePos[1] < terrain.maxBounds.y &&
            checkedCubePos[2] >= 0 && checkedCubePos[2] < terrain.maxBounds.z)
        {
            if (Database.Instance.cubesDatas
                [terrain.matrix[checkedCubePos[0], checkedCubePos[1], checkedCubePos[2]].id].isSolid)
            {
                movementPos[0] = checkedCubePos[0];
                movementPos[1] = checkedCubePos[1] + 1;
                movementPos[2] = checkedCubePos[2];
                canMove = true;
                for (int i = 1; canMove == true && i <= entityHeight; i++)
                {
                    if (Database.Instance.cubesDatas
                    [terrain.matrix[checkedCubePos[0], checkedCubePos[1] + i, checkedCubePos[2]].id].isSolid)
                    {
                        canMove = false;
                        movementPos[0] = -1;
                        movementPos[1] = -1;
                        movementPos[2] = -1;
                    }
                }
            }
            else
            {
                if (Database.Instance.cubesDatas
                [terrain.matrix[checkedCubePos[0], checkedCubePos[1]-1, checkedCubePos[2]].id].isSolid)
                {
                    canMove = true;
                    movementPos[0] = checkedCubePos[0];
                    movementPos[1] = checkedCubePos[1];
                    movementPos[2] = checkedCubePos[2];
                }
            }
        }

        possibleMovementPos = movementPos;
        return canMove;
    }

    private void Die()
    {
        if (healthBarManager != null)
        {
            healthBarManager.DeRegisterHealthBar(this);
        }
    }

    private void SetPosition(int[] posToSet)
    {
        facesToScan.Clear();

        if (pos[0] < posToSet[0])
        {
            facesToScan.Add(ViewFace.Right);
        }
        else if (pos[0] > posToSet[0])
        {
            facesToScan.Add(ViewFace.Left);
        }

        if (pos[1] < posToSet[1])
        {
            facesToScan.Add(ViewFace.Top);
        }
        else if (pos[1] > posToSet[1])
        {
            facesToScan.Add(ViewFace.Bot);
        }

        if (pos[2] < posToSet[2])
        {
            facesToScan.Add(ViewFace.Front);
        }
        else if (pos[2] > posToSet[2])
        {
            facesToScan.Add(ViewFace.Back);
        }

        StartCoroutine(MoveCor(new Vector3(posToSet[0] + positionOffset.x,
                                         posToSet[1] + positionOffset.y,
                                         posToSet[2] + positionOffset.z)));
        pos = posToSet;
    }

    #endregion
}

public static class EntityData
{
    public static int[,] directionalOffsets = new int[6, 3]
    {
        {0, 0, 1}, //front
        {0, 0, -1}, //back
        {0, 1, 0}, //up
        {0, -1, 0}, //down
        {1, 0, 0}, //right
        {-1, 0, 0}, //left
    };

    public static int[] GetDirectionalPos(EntityMovementDirection dir, int[] entityPos)
    {
        int[] requestedPos = new int[3]
        {
            directionalOffsets[(int)dir,0] + entityPos[0],
            directionalOffsets[(int)dir,1] + entityPos[1],
            directionalOffsets[(int)dir,2] + entityPos[2],
        };

        return requestedPos;
    }
}

public enum EntityMovementDirection
{
    Front,
    Back,
    Up,
    Down,
    Right,
    Left,
}
