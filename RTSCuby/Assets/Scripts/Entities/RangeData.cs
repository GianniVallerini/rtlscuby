﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RangeData
{
    public static int baseRange = 10; //must be positive and even

    public static Vector3[] viewVerts = new Vector3[8]
    {
        new Vector3(-(baseRange/2), (baseRange/2), (baseRange/2)),  //top left front
        new Vector3((baseRange/2), (baseRange/2), (baseRange/2)),   //top right front
        new Vector3((baseRange/2), -(baseRange/2), (baseRange/2)),  //bot right front
        new Vector3(-(baseRange/2), -(baseRange/2), (baseRange/2)), //bot left front
        new Vector3(-(baseRange/2), (baseRange/2), -(baseRange/2)), //top left back
        new Vector3((baseRange/2), (baseRange/2), -(baseRange/2)),  //top right back
        new Vector3((baseRange/2), -(baseRange/2), -(baseRange/2)), //bot right back
        new Vector3(-(baseRange/2), -(baseRange/2), -(baseRange/2)),//bot left back
    };

    public static int[,] viewFaces = new int[6, 2]
    {
        {3, 1},  //front
        {7, 5},  //back
        {4, 1},  //top
        {7, 2},  //bot
        {6, 1},  //right
        {7, 0},  //left
    };

    public static int[] facesNotInterestedAxis = new int[6] {2,2,1,1,3,3};

    public static Dictionary<Vector3, InteractableCubeData> Scan(int[] entityPos, WorldTerrain terrain)
    {
        Dictionary<Vector3, InteractableCubeData> returningDict = new Dictionary<Vector3, InteractableCubeData>();

        for (int x = entityPos[0] - (baseRange / 2); x <= entityPos[0] + (baseRange / 2); x++)
        {
            for (int y = entityPos[1] - (baseRange / 2); y <= entityPos[1] + (baseRange / 2); y++)
            {
                for (int z = entityPos[2] - (baseRange / 2); z <= entityPos[2] + (baseRange / 2); z++)
                {
                    if (x > 0 && x < terrain.maxBounds.x &&
                       y > 0 && y < terrain.maxBounds.y &&
                       z > 0 && z < terrain.maxBounds.z)
                    {
                        InteractableCubeData interactable = Database.Instance.cubesDatas[terrain.matrix[x, y, z].id] as InteractableCubeData;
                        if (interactable != null)
                        {
                            returningDict.Add(new Vector3(x,y,z), interactable);
                        }
                    }
                }
            }
        }

        return returningDict;
    }

    public static Dictionary<Vector3, InteractableCubeData> Scan(int[] entityPos, WorldTerrain terrain,
                                                                 Dictionary<Vector3, InteractableCubeData> currDict,
                                                                 Vector3 cubePosition)
    {
        Dictionary<Vector3, InteractableCubeData> returningDict = currDict;
        InteractableCubeData interactable = 
            Database.Instance.cubesDatas[terrain.matrix[(int)cubePosition.x, (int)cubePosition.y, (int)cubePosition.z].id] 
            as InteractableCubeData;
        if (interactable != null)
        {
            returningDict[cubePosition] = interactable;
        }
        else
        {
            returningDict.Remove(cubePosition);
        }

        return returningDict;
    }

    public static Dictionary<Vector3, InteractableCubeData> Scan(int[] entityPos, WorldTerrain terrain,
                                                                 Dictionary<Vector3, InteractableCubeData> currDict, 
                                                                 ViewFace viewFace)
    {
        Dictionary<Vector3, InteractableCubeData> returningDict = currDict;

        int oppositeFace = (int)System.Enum.GetValues(typeof(OppositeFace)).GetValue((int)viewFace);

        Vector3[] faceMinMaxVerts = new Vector3[2]
        {
            viewVerts[viewFaces[(int)viewFace, 0]],
            viewVerts[viewFaces[(int)viewFace, 1]],
        };

        //Check the face i'm moving towards
        int interestedAxis = facesNotInterestedAxis[(int)viewFace];

        int minX = (int)(entityPos[0] + faceMinMaxVerts[0].x);
        int maxX = (int)(entityPos[0] + faceMinMaxVerts[1].x);
        int minY = (int)(entityPos[1] + faceMinMaxVerts[0].y);
        int maxY = (int)(entityPos[1] + faceMinMaxVerts[1].y);
        int minZ = (int)(entityPos[2] + faceMinMaxVerts[0].z);
        int maxZ = (int)(entityPos[2] + faceMinMaxVerts[1].z);

        for (int x = minX; x <= maxX; x++)
        {
            for(int y = minY; y <= maxY; y++)
            {
                for(int z = minZ; z <= maxZ; z++)
                {
                    InteractableCubeData interactable =
                        Database.Instance.cubesDatas[terrain.matrix[x, y, z].id]
                        as InteractableCubeData;
                    if (interactable != null)
                    {
                        returningDict[new Vector3(x, y, z)] = interactable;
                    }
                }
            }
        }

        //Clear the face i'm moving away from

        List<Vector3> keysToRemove = new List<Vector3>();
        if (minX == maxX)
        {
            minX = (int)(entityPos[0] - faceMinMaxVerts[0].x - Mathf.Sign(faceMinMaxVerts[0].x));
            foreach (KeyValuePair<Vector3, InteractableCubeData> pair in returningDict)
            {
                if (pair.Key.x == minX)
                {
                    keysToRemove.Add(pair.Key);
                }
            }
        }
        else if (minY == maxY)
        {
            minY = (int)(entityPos[1] - faceMinMaxVerts[0].y - Mathf.Sign(faceMinMaxVerts[0].y));
            foreach (KeyValuePair<Vector3, InteractableCubeData> pair in returningDict)
            {
                if (pair.Key.y == minY)
                {
                    keysToRemove.Add(pair.Key);
                }
            }
        }
        else if (minZ == maxZ)
        {
            minZ = (int)(entityPos[2] - faceMinMaxVerts[0].z - Mathf.Sign(faceMinMaxVerts[0].z));
            foreach (KeyValuePair<Vector3, InteractableCubeData> pair in returningDict)
            {
                if (pair.Key.z == minZ)
                {
                    keysToRemove.Add(pair.Key);
                }
            }
        }

        foreach (Vector3 key in keysToRemove)
        {
            returningDict.Remove(key);
        }

        return returningDict;
    }
}

public enum ViewFace
{
    Front = 0,
    Back = 1,
    Top = 2,
    Bot = 3,
    Right = 4,
    Left = 5,
}

public enum OppositeFace
{
    Front = 1,
    Back = 0,
    Top = 3,
    Bot = 2,
    Right = 5,
    Left = 4,
}