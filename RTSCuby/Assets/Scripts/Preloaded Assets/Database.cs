﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Database", menuName = "Database")]
public class Database : SerializedSingletonScriptable<Database>
{
    #region Fields

    [DictionaryDrawerSettings(DisplayMode = DictionaryDisplayOptions.OneLine, KeyLabel = "Cube IDs", ValueLabel = "Cube Datas")]
    public Dictionary<short, CubeData> cubesDatas = new Dictionary<short, CubeData>();

    #endregion

    #region Unity Callbacks

    #endregion

    #region Methods

    #endregion
}
