﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Settings", menuName = "Settings")]
public class Settings : SerializedSingletonScriptable<Settings>
{
    #region Fields

    [Title("Debug Preferences")]
    public bool showEntitiesViewRange = true;
    public Color entitiesViewRangeColor = Color.white;
    public bool showEntitiesPossibleTargets = true;
    public Color entitiesPossTargetsColor = Color.white;
    public bool showEntitiesCurrentTarget = true;
    public Color entitiesCurrTargetColor = Color.white;

    #endregion

    #region Unity Callbacks

    #endregion

    #region Methods

    #endregion
}
