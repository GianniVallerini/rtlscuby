﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer)), RequireComponent(typeof(MeshFilter))]
public class Chunk : MonoBehaviour
{
    #region Fields

    public MeshRenderer meshRenderer;
    public MeshFilter meshFilter;

    private int vertexIndex = 0;
    private List<Vector3> vertices = new List<Vector3>();
    private List<int> triangles = new List<int>();
    private List<Vector2> uvs = new List<Vector2>();

    private short[,,] voxelMap = new short[VoxelData.chunkWidth, VoxelData.chunkHeight, VoxelData.chunkWidth];
    private Vector3 chunkOffset = new Vector3();
    private WorldTerrain worldTerrain = new WorldTerrain();

    #endregion

    #region Unity Callbacks

    #endregion

    #region Methods

    public void Initialize(Vector3 chunkoffset, WorldTerrain worldterrain)
    {
        chunkOffset = chunkoffset;
        worldTerrain = worldterrain;
        PopulateVoxelMap();
        CreateChunkVoxelData();
        CreateMesh();
    }

    private void AddVoxelDataToChunk(Vector3 pos)
    {
        int offsetX = Mathf.FloorToInt(chunkOffset.x);
        int offsetY = Mathf.FloorToInt(chunkOffset.y);
        int offsetZ = Mathf.FloorToInt(chunkOffset.z);
        for (int i = 0; i < 6; i++)
        {
            if (!IsVoxelSolid(pos + VoxelData.faceChecks[i]))
            {
                //vertex optimization
                vertices.Add(pos + VoxelData.VoxelVerts[VoxelData.VoxelTris[i, 0]]);
                vertices.Add(pos + VoxelData.VoxelVerts[VoxelData.VoxelTris[i, 1]]);
                vertices.Add(pos + VoxelData.VoxelVerts[VoxelData.VoxelTris[i, 2]]);
                vertices.Add(pos + VoxelData.VoxelVerts[VoxelData.VoxelTris[i, 3]]);
                CubeData cubeData = Database.Instance.cubesDatas[worldTerrain.matrix[(int)pos.x + offsetX, (int)pos.y + offsetY, (int)pos.z + offsetZ].id];
                AddTexture(cubeData.GetTextureID(i));
                triangles.Add(vertexIndex);
                triangles.Add(vertexIndex + 1);
                triangles.Add(vertexIndex + 2);
                triangles.Add(vertexIndex + 2);
                triangles.Add(vertexIndex + 1);
                triangles.Add(vertexIndex + 3);
                vertexIndex += 4;
            }
        }
    }

    private void CreateChunkVoxelData()
    {
        for (int x = 0; x < VoxelData.chunkWidth; x++)
        {
            for (int z = 0; z < VoxelData.chunkWidth; z++)
            {
                for (int y = 0; y < VoxelData.chunkHeight; y++)
                {
                    if (IsVoxelDrawable(new Vector3(x, y, z)))
                    {
                        AddVoxelDataToChunk(new Vector3(x, y, z));
                    }
                }
            }
        }
    }

    private void PopulateVoxelMap()
    {
        int offsetX = Mathf.FloorToInt(chunkOffset.x);
        int offsetY = Mathf.FloorToInt(chunkOffset.y);
        int offsetZ = Mathf.FloorToInt(chunkOffset.z);
        for (int x = offsetX; x < VoxelData.chunkWidth + offsetX; x++)
        {
            for (int z = offsetZ; z < VoxelData.chunkWidth + offsetZ; z++)
            {
                for (int y = offsetY; y < VoxelData.chunkHeight + offsetY; y++)
                {
                    voxelMap[x - offsetX, y - offsetY, z - offsetZ] = worldTerrain.matrix[x, y, z].id;
                }
            }
        }
    }

    private bool IsVoxelSolid(Vector3 pos)
    {
        int x = Mathf.FloorToInt(pos.x);
        int y = Mathf.FloorToInt(pos.y);
        int z = Mathf.FloorToInt(pos.z);

        if (x < 0 || x > VoxelData.chunkWidth - 1 ||
            y < 0 || y > VoxelData.chunkHeight - 1 ||
            z < 0 || z > VoxelData.chunkWidth - 1)
        {
            return false;
        }
        return Database.Instance.cubesDatas[voxelMap[x, y, z]].isSolid;
    }

    private bool IsVoxelDrawable(Vector3 pos)
    {
        int x = Mathf.FloorToInt(pos.x);
        int y = Mathf.FloorToInt(pos.y);
        int z = Mathf.FloorToInt(pos.z);

        return Database.Instance.cubesDatas[voxelMap[x, y, z]].isDrawable;
    }

    private void CreateMesh()
    {
        Mesh mesh = new Mesh
        {
            vertices = vertices.ToArray(),
            triangles = triangles.ToArray(),
            uv = uvs.ToArray()
        };

        mesh.RecalculateNormals();

        meshFilter.mesh = mesh;
    }

    private void AddTexture(int textureID)
    {
        float y = textureID / VoxelData.textureAtlasSizeInBlocks;
        float x = textureID - (y * VoxelData.textureAtlasSizeInBlocks);

        x *= VoxelData.NormalizedBlocksTextureSize;
        y *= VoxelData.NormalizedBlocksTextureSize;

        y = 1f - y - VoxelData.NormalizedBlocksTextureSize;

        uvs.Add(new Vector2(x, y));
        uvs.Add(new Vector2(x, y + VoxelData.NormalizedBlocksTextureSize));
        uvs.Add(new Vector2(x + VoxelData.NormalizedBlocksTextureSize, y));
        uvs.Add(new Vector2(x + VoxelData.NormalizedBlocksTextureSize, y + VoxelData.NormalizedBlocksTextureSize));
    }

    public void ActivateSurfaceCubes()
    {
        bool surfaceFound = false;
        for (int x = 0; x < VoxelData.chunkWidth; x++)
        {
            for (int z = 0; z < VoxelData.chunkWidth; z++)
            {
                surfaceFound = false;
                for (int y = VoxelData.chunkHeight - 1; !surfaceFound && y >= 0; y--)
                {
                    if (voxelMap[x, y, z] != 0)
                    {
                        Database.Instance.cubesDatas[voxelMap[x, y, z]].Initialize(new Vector3(x + chunkOffset.x,
                                                                                               y + chunkOffset.y,
                                                                                               z + chunkOffset.z));
                        surfaceFound = true;
                    }
                }
            }
        }
    }

    #endregion
}
